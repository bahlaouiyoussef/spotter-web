import Link from "next/link";
import Button from "../components/Button";
import { Navbar, NavbarItem, ItemSeparator } from "../components/Navbar";

function Layout(props) {
  var { className, children, footerFixed } = props;
  var footerClassName =
    "flex h-40 p-5 w-full bg-gray-100 items-center justify-center";

  if (footerFixed) {
    footerClassName += " fixed bottom-0 left-0";
  }

  return (
    <main className={`${className} text-primary`}>
      <Navbar>
        <NavbarItem>
          <Link href={"/"}>Home</Link>
        </NavbarItem>

        <ItemSeparator />

        <NavbarItem>
          <Link href={"/signup"}>
            <Button>Sign up</Button>
          </Link>
        </NavbarItem>
        <NavbarItem>
          <Link href={"/signin"}>Sign in</Link>
        </NavbarItem>
      </Navbar>

      {children}

      <footer className={footerClassName}>
        <span>&copy; 2020 Spotter, All rights reserved.</span>
      </footer>
    </main>
  );
}

export default Layout;
