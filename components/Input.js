import { useState } from "react";
import EyeIcon from "./icons/outline/Eye";
import EyeOffIcon from "./icons/outline/EyeOff";

function Input(props, ref) {
  var [showPassword, setShowPassword] = useState(false);

  var { className, password, ...rest } = props;
  const tailwindStyle =
    "appearance-none rounded relative px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm";

  var Component = EyeIcon;
  if (showPassword) {
    Component = EyeOffIcon;
  }

  var inputType = "text";
  if (password && !showPassword) {
    inputType = "password";
  }

  function showPassword_click() {
    setShowPassword(!showPassword);
  }

  return (
    <div className={"relative"}>
      <input
        ref={ref}
        type={inputType}
        className={`${tailwindStyle} ${className}`}
        {...rest}
      />
      {password && (
        <Component
          className={"absolute right-2 top-2 z-1 w-6 cursor-pointer"}
          onClick={showPassword_click}
        />
      )}
    </div>
  );
}

export default React.forwardRef(Input);
