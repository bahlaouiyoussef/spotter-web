module.exports = {
  async redirects() {
    return [
      {
        source: "/signin",
        destination: "/coming-soon",
        permanent: false
      },
      {
        source: "/signup",
        destination: "/coming-soon",
        permanent: false
      }
    ];
  }
};
